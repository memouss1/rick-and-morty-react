
export default function Characters(props) {
    // ! 10.- verificar que recibo la data
    // ! 15.- Traer la información de la data
    const { characters, setCharacters } = props;
    console.log(props);
  return (
    <div className="characters">
      <h1>Personajes</h1>
      {/*  16.- Agregar span, para llevar al home */}
      <span className="back-home"> volver al inicio</span>
    {/* 17.- Agregar el div que contendrá a los personajes */}
      <div className="container-characters">
        {/* 18.- recorrer la data */}
        {characters.map((characters, index) =>(
          <div className="character-container" key={index}>
            {/* 19.- Recibimos la data */}
            <div className="">
              <img src={characters.image} alt={characters.name} />
            </div>
            <div className="">
              <h3>{characters.name}</h3>
            </div>
            {/* 20.- Condicionar status */}
            <div className="">
              <h6>
                {characters.status === "Alive" ? (
                    <>
                      <span className="alive" />
                      Alive
                    </>
                ) : (

                    <>
                    <span className="dead" />
                    Dead
                     </>
                )
                
                }
              </h6>
            </div>
          </div>
        ))}
      </div>
    </div> 
  )
}
