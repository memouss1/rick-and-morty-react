//? 2.- Importar imagen
import imgRick from './assets/img/rick-morty.png'
// ? 5.- Importar useState
import { useState } from 'react'
import Characters from './Components/Characters';
function App() {  
 
  //! 5.- definir el state
  const [characters, setCharacters] = useState(null) 


  // ! 4.- Hacer método que hará petición a la Api
  const clicApi = async () => {

    // ! 4.-1 Hacer la petición a la API
    const api = await fetch('https://rickandmortyapi.com/api/character');
    const personajesApi = await api.json();

    // console.log(personajesApi);
    // ! 7.- Setear datos de la api
    setCharacters(personajesApi.results);

    // console.log(characters);

    // ! 6.- Hacer console.log del state

  }

  return (
    <div className="App">
      <header>
        {/* 1.- Titlo de la App */}
        <h1 className='title'>Rick & Morty</h1>

        {/* 8.- Crar componente Characters */}
        {/* <Characters characters={characters} /> */}
        {/* 9.- pasar data através de props al componente Characters */}
        
        {/* 11.- Condicionar la data */}
        {
          // ? 12.- Si la data es correcta, mostrar characters
        characters ? (
          <Characters characters={characters} />
        ) : (
          //? 13.- si la data es incorrecta, nos dejaá en el home
          //? 14.- Utilizar fragment para mostrar el home
          // <h2>Error</h2>
          <>
            <img className='img-home' src={imgRick} alt="" />
            <button onClick={clicApi} className='btn-search'> Buscar Personaje </button>

          </>
        )
        }

        {/* 2.- Invocar Imagen */}
        {/* <img className='img-home' src={imgRick} alt="" /> */}

        {/* 3.- Agregar boton busqueda */}
        {/* <button onClick={clicApi} className=''> Buscar Personaje </button> */}
      </header>
    </div>
  )
}

export default App
